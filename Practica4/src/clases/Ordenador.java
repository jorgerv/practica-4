package clases;

import java.time.LocalDate;

public class Ordenador {
	
	//atributos
	private String codOrdenador;
	private String marca;
	private String modelo;
	private double precio;
	private int tamannoRam;
	private int nucleosProcesador;
	private LocalDate fechaFabricacion;
	private Cliente clientes;
	
	
	//setter y getter
	public String getCodOrdenador() {
		return codOrdenador;
	}
	public void setCodOrdenador(String codOrdenador) {
		this.codOrdenador = codOrdenador;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getTamannoRam() {
		return tamannoRam;
	}
	public void setTamannoRam(int tamannoRam) {
		this.tamannoRam = tamannoRam;
	}
	public int getNucleosProcesador() {
		return nucleosProcesador;
	}
	public void setNucleosProcesador(int nucleosProcesador) {
		this.nucleosProcesador = nucleosProcesador;
	}
	public LocalDate getFechaFabricacion() {
		return fechaFabricacion;
	}
	public void setFechaFabricacion(LocalDate fechaFabricacion) {
		this.fechaFabricacion = fechaFabricacion;
	}
	
	public Cliente getClientes() {
		return clientes;
	}
	public void setClientes(Cliente clientes) {
		this.clientes = clientes;
	}
	
	
	
	
	//constructores
	
	
	/**
	 * Constructor de ordenador con todos los paramteros
	 * @param codOrdenador id del ordenador
	 * @param marca marca del ordenador
	 * @param modelo modelo del ordenador
	 * @param precio precio del ordenador
	 * @param tamannoRam tama�o de la ram
	 * @param nucleosProcesador nucleos del procesador
	 * @param fechaFabricacion fecha de fabricacion del ordenador
	 */
	public Ordenador(String codOrdenador, String marca, String modelo, double precio, int tamannoRam,
			int nucleosProcesador, String fechaFabricacion) {
		this.codOrdenador = codOrdenador;
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.tamannoRam = tamannoRam;
		this.nucleosProcesador = nucleosProcesador;
		this.fechaFabricacion = LocalDate.parse(fechaFabricacion);
	}
	
	
	/**
	 * Constructor de ordenador vacio
	 */
	public Ordenador() {
		
	}
	
	
	/**
	 * Constructor de ordenador con un solo parametro
	 * @param codOrdenador id del ordenador
	 */
	public Ordenador(String codOrdenador) {
		this.codOrdenador = codOrdenador;
	}
	
	//toString
	@Override
	public String toString() {
		return "Ordenador: CodigoOrdenador=" + codOrdenador + ", Marca=" + marca + ", Modelo=" + modelo + ", Precio="
				+ precio + ", Tama�oRam=" + tamannoRam + ", NucleosProcesador="
				+ nucleosProcesador + ", FechaFabricacion=" + fechaFabricacion+ "\nDatos del cliente="+clientes+"\n";
	}
}
