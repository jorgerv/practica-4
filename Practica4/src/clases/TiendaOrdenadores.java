package clases;

public class TiendaOrdenadores {

	// atributos
	private Ordenador[] ordenadores;

	// constructor

	/**
	 * Constructor de tienda de ordenadores con un parametro
	 * 
	 * @param ordenadoresVendidos numero de ordenadores vendidos
	 */
	public TiendaOrdenadores(int ordenadoresVendidos) {
		this.ordenadores = new Ordenador[ordenadoresVendidos];
	}

	// metodos

	// altaOrdenador
	
	/**
	 * Metodo para dar de alta los ordenadores vendidos
	 * @param codOrdenador id de ordenador
	 * @param marca marca del ordenador
	 * @param modelo modelo del ordenador
	 * @param precio precio del ordenador
	 * @param tamannoRam tama�o de la ram
	 * @param nucleosProcesador nucleos del procesador
	 * @param fechaFabricacion fecha de fabricacion del ordenador
	 * @param clientes cliente que compro el ordenador
	 */
	public void altaOrdenador(String codOrdenador, String marca, String modelo, double precio, int tamannoRam,
			int nucleosProcesador, String fechaFabricacion, Cliente clientes) {

		for (int i = 0; i < ordenadores.length; i++) {
			if (ordenadores[i] == null) {
				ordenadores[i] = new Ordenador(codOrdenador, marca, modelo, precio, tamannoRam, nucleosProcesador,
						fechaFabricacion);
				ordenadores[i].setClientes(clientes);
				break;
			}
		}
	}

	// buscar por codigo de ordenador
	/**
	 * Metodo para buscar un ordenador mediante id de ordenador
	 * @param codOrdenador id del ordenador
	 * @return devuelve el vector de ordenadores
	 */
	public Ordenador buscarOrdenadorPorOrdenador(String codOrdenador) {
		for (int i = 0; i < ordenadores.length; i++) {
			if (ordenadores[i] != null) {
				if (ordenadores[i].getCodOrdenador().equals(codOrdenador)) {
					return ordenadores[i];
				}
			}
		}
		return null;
	}

	// buscar por cliente
	/**
	 * Metodo para buscar ordenador mediante id de compra
	 * @param cliente id de compra
	 * @return devuelve el vector de ordenadores
	 */
	public Ordenador buscarOrdenadorPorCliente(Cliente cliente) {
		for (int i = 0; i < ordenadores.length; i++) {
			if (ordenadores[i] != null) {
				if (ordenadores[i].getClientes().equals(cliente)) {
					return ordenadores[i];
				}
			}
		}
		return null;
	}

	// eliminar ordenador por codigo de ordenador
	/**
	 * Metodo para eliminar un ordenador mediante id de ordenador
	 * @param codOrdenador id de ordenador
	 */
	public void eliminarOrdenador(String codOrdenador) {
		for (int i = 0; i < ordenadores.length; i++) {
			if (ordenadores[i] != null) {
				if (ordenadores[i].getCodOrdenador().equals(codOrdenador)) {
					System.out.println("Ordenador eliminado");
					ordenadores[i] = null;
				}
			}
		}

	}

	// listar ordenadores
	/**
	 * Metodo para listar todos los ordenadores
	 */
	public void listarOrdenadores() {
		for (int i = 0; i < ordenadores.length; i++) {
			if (ordenadores[i] != null) {
				System.out.println(ordenadores[i]);
			}
		}
	}

	// cambiar marca ordenador
	/**
	 * Metodo para cambiar la marca de un ordenador mediante id de ordenador
	 * @param codOrdenador id de ordenador
	 * @param nuevaMarca nueva marca
	 */
	public void cambiarMarca(String codOrdenador, String nuevaMarca) {
		for (int i = 0; i < ordenadores.length; i++) {
			if (ordenadores[i] != null) {
				if (ordenadores[i].getCodOrdenador().equals(codOrdenador)) {
					ordenadores[i].setMarca(nuevaMarca);
				}
			}
		}
	}

	// listar por marca de ordenador
	/**
	 * Metodo para listar ordenadores por la marca
	 * @param nombreOrdenador marca de ordenador
	 */
	public void listarPorMarca(String nombreOrdenador) {
		for (int i = 0; i < ordenadores.length; i++) {
			if (ordenadores[i] != null) {
				if (ordenadores[i].getMarca().equalsIgnoreCase(nombreOrdenador)) {
					System.out.println(ordenadores[i]);
				}
			}
		}
	}

	// ordenar por mas barato
	/**
	 * Metodo para ordenar de barato a caro
	 */
	public void ordenarPorPrecioBarato() {
		for (int i = 0; i < ordenadores.length - 1; i++) {
			if(ordenadores[i]!=null) {
				for (int j = i + 1; j < ordenadores.length; j++) {
					if(ordenadores[j]!=null) {
						if (ordenadores[j].getPrecio() < ordenadores[i].getPrecio()) {
							Ordenador aux = ordenadores[i];
							ordenadores[i] = ordenadores[j];
							ordenadores[j] = aux;
						}
					}
				}
			}
		}
			for (int i = 0; i < ordenadores.length; i++) {
				if(ordenadores[i]!=null) {
					System.out.println(ordenadores[i]);
				}
			}
	}

	// ordenador por mas caro
	/**
	 * Metodo para de mas caro a barato
	 */
	public void ordenarPorPrecioCaro() {
		for (int i = 0; i < ordenadores.length - 1; i++) {
			if(ordenadores[i]!=null) {
				for (int j = i + 1; j < ordenadores.length; j++) {
					if (ordenadores[j]!=null) {
						if (ordenadores[j].getPrecio() > ordenadores[i].getPrecio()) {
							Ordenador aux = ordenadores[i];
							ordenadores[i] = ordenadores[j];
							ordenadores[j] = aux;
						}
					}
				}
			}
		}
			
		for (int i = 0; i < ordenadores.length; i++) {
			if(ordenadores[i]!=null) {
				System.out.println(ordenadores[i]);
			}
		}
	}

}
