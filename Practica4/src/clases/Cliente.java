package clases;

import java.time.LocalDate;

public class Cliente {

	//atributos
	private String codCompra;
	private String nombre;
	private LocalDate fechaCompra;
	private String dni;
	
	
	
	//setter y getter
	public String getCodCompra() {
		return codCompra;
	}
	public void setCodCompra(String codCompra) {
		this.codCompra = codCompra;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LocalDate getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	
	/**
	 * Constructor de cliente vacio
	 */
	//constructores
	public Cliente() {
		
	}
	
	
	/**
	 * Constructor de cliente con un parametro
	 * @param codCompra id de la compra
	 */
	public Cliente(String codCompra) {
		this.codCompra=codCompra;
	}
	
	
	/**
	 * Constructor cliente con todos los parametros
	 * @param codCompra id de la compra
	 * @param nombre nombre del cliente
	 * @param fechaCompra echa de la compra
	 * @param dni dni del cliente
	 */
	public Cliente(String codCompra, String nombre, String fechaCompra, String dni) {
		this.codCompra = codCompra;
		this.nombre = nombre;
		this.fechaCompra = LocalDate.parse(fechaCompra);
		this.dni = dni;
	}
	
	
	
	//toString
	@Override
	public String toString() {
		return "Clientes CodigoCompra=" + codCompra + ", Nombre=" + nombre + ", FechaCompra=" + fechaCompra + ", Dni="
				+ dni;
	}
}
