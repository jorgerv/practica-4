package programa;


import clases.TiendaOrdenadores;

import java.util.Scanner;

import clases.Cliente;


public class Programa {

	public static void main(String[] args) {
		
		Scanner teclado=new Scanner(System.in);
		
		int ordenadoresVendidos=5;
		
		 TiendaOrdenadores miTienda=new TiendaOrdenadores(ordenadoresVendidos);
		
		 //clientes de la tienda
		 Cliente cliente1=new Cliente("1","Alberto",("2021-12-21"),"43564096A");
		 Cliente cliente2=new Cliente("2","Miguel",("2022-01-12"),"96837328S");
		 Cliente cliente3=new Cliente("3","Jorge",("2021-11-21"),"49452096L");
		 Cliente cliente4=new Cliente("4","Maria",("2022-02-10"),"98548738K");
		 Cliente cliente5=new Cliente("5","Jesus",("2021-09-11"),"86829064X");
		 
		 //ordenadores comprados por los clientes
		 miTienda.altaOrdenador("124", "MSI", "Mag codex 5", 1350, 16, 6, ("2019-10-25"), cliente1);
		 miTienda.altaOrdenador("362", "Lenovo", "Pavilion Desktop TP01", 639.99, 16, 6, ("2020-09-12"), cliente2);
		 miTienda.altaOrdenador("2564", "Lenovo", "IdeaCentre 5", 479.98, 8, 6, ("2018-10-09"), cliente3);
		 miTienda.altaOrdenador("64678", "Acer", "Predator Orion 3000", 1829, 32, 8, ("2018-05-13"), cliente4);
		 miTienda.altaOrdenador("32", "Apple", "iMac Apple M1", 1312.34, 8, 8, ("2019-08-24"), cliente5);
		 
		 
		 
		
		 int opcionMenu=0;
		 int opcionBuscar=0;
		 int opcionOrdenar=0;
		 
		 do {
			 System.out.println("*****TIENDA DE ORDENADORES*****");
			 System.out.println("1-Buscar ordenador");
			 System.out.println("2-Eliminar ordenador");
			 System.out.println("3-Listar ordenadores");
			 System.out.println("4-Listar ordenadores por marca");
			 System.out.println("5-Cambiar marca ordenador");
			 System.out.println("6-Ordenar ordenadores por precio");
			 System.out.println("7-Salir");
			 opcionMenu=teclado.nextInt();
			 switch (opcionMenu) {
			 case 1:
				 do {
					 System.out.println("****************");
					 System.out.println("1-Buscar por codigo de ordenador");
					 System.out.println("2-Buscar por codigo de compra");
					 System.out.println("3-Volver al menu principal");
					 System.out.println("****************");
					 opcionBuscar=teclado.nextInt();
					 switch (opcionBuscar) {
					 case 1:
						 System.out.println("Introduce el codigo del ordenador");
						 teclado.nextLine();
						 String codOrdenador=teclado.nextLine();
						 System.out.println(miTienda.buscarOrdenadorPorOrdenador(codOrdenador));
						 
						 break;
					 case 2:
						 System.out.println("Introduce el codigo de compra");
						 teclado.nextLine();
						 String codCompra=teclado.nextLine();
						 if(codCompra.equals(cliente1.getCodCompra())) {
							 System.out.println(miTienda.buscarOrdenadorPorCliente(cliente1));
						 }else if(codCompra.equals(cliente2.getCodCompra())) {
							 System.out.println(miTienda.buscarOrdenadorPorCliente(cliente2));
						 }else if(codCompra.equals(cliente3.getCodCompra())) {
							 System.out.println(miTienda.buscarOrdenadorPorCliente(cliente3));
						 }else if(codCompra.equals(cliente4.getCodCompra())) {
							 System.out.println(miTienda.buscarOrdenadorPorCliente(cliente4));
						 }else if(codCompra.equals(cliente5.getCodCompra())) {
							 System.out.println(miTienda.buscarOrdenadorPorCliente(cliente5));
						 }else {
							 System.out.println("El codigo de compra introducido es incorrecto");
						 }
						 System.out.println();
						 break;
						 
						
					 case 3:
						 System.out.println("Volviendo al menu principal");
						 System.out.println();
						 break;
					default:
						System.out.println("La opcion introducida no es valida");
						System.out.println();
						break;
					 }
				 }while(opcionBuscar!=3);
				 break;
				 
			 case 2:
				 System.out.println("Introduce el codigo del ordenador que quieres eliminar");
				 teclado.nextLine();
				 String codOrdenadorEliminar=teclado.nextLine();
				 miTienda.eliminarOrdenador(codOrdenadorEliminar);
				 System.out.println();
				 break;
				 
			 case 3:
				miTienda.listarOrdenadores();
				System.out.println();
				break;
				
			 case 4:
				 System.out.println("Introduce la marca del ordenador");
				 teclado.nextLine();
				 String marcaIntroducida=teclado.nextLine();
				 miTienda.listarPorMarca(marcaIntroducida);
				 break;
				 
			 case 5:
				 System.out.println("Introduce el codigo del ordenador a cambiar");
				 teclado.nextLine();
				 String codOrdenadorCambiar=teclado.nextLine();
				 System.out.println("Introduce la nueva marca del ordenador");
				 String marcaNueva=teclado.nextLine();
				 miTienda.cambiarMarca(codOrdenadorCambiar, marcaNueva);
				 System.out.println();
				 break;
				 
			
			 case 6:
				 do {
					 System.out.println("**********");
					 System.out.println("1-Ordenar de barato a caro");
					 System.out.println("2-Ordenar de caro a barato");
					 System.out.println("3-Volver al menu principal");
					 System.out.println("**********");
					 opcionOrdenar=teclado.nextInt();
					 
					 switch (opcionOrdenar) {
					 case 1:
						 miTienda.ordenarPorPrecioBarato();
						 System.out.println();
						 break;
					 case 2:
						 miTienda.ordenarPorPrecioCaro();
						 System.out.println();
						 break;
					 case 3:
						 System.out.println("Volviendo al menu principal");
						 break;
					default:
						System.out.println("La opcion introducida no es correcta");
						break;
					 }
				 }while(opcionOrdenar!=3);
				 break;
				 
			 case 7:
				 System.out.println("Adios");
				 break;
				 
			default:
				System.out.println("La opcion introducida no es correcta");
				System.out.println();
				break;
			 }
			 
			
		 }while(opcionMenu!=7);
		 
			 
		teclado.close();
	}

}
